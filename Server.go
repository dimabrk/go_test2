package main

import (
	"bufio"
	"fmt"
	"io"
	"net"
	"net/http"
	"os"
	"strings"
	"time"
	//"github.com/claudiu/gocron"
)

type serviceInfo struct {
	name        string
	ping        time.Duration
	pingIsCheck bool
}

var servicesInfo []serviceInfo
var commandStatistic map[string]int

func main() {
	commandStatistic = make(map[string]int)
	setServisInfo()
	//s := gocron.NewScheduler()
    //s.Every(1).Minutes.Do(pingSitesThread())
    //<- s.Start()
	go pingSitesThread()

	listener, err := net.Listen("tcp", ":4545")

	if err != nil {
		panic(err)
	}

	defer listener.Close()

	for {
		conn, err := listener.Accept()

		if err != nil {
			fmt.Println(err)
			conn.Close()
			continue
		}

		go handleConnection(conn)
	}
}

func handleConnection(conn net.Conn) {
	defer conn.Close()

	for {
		input := make([]byte, 4096)
		n, err := conn.Read(input)

		if n == 0 || err != nil {
			fmt.Println("Error read", err)
			break
		}

		command := string(input[0:n])

		fmt.Println("Command: " + command)

		var response string

		switch command {
		case "max_ping":
			servis, ping := getMaxPing()
			response = "servis: " + servis + "; ping: " + ping.String()
		case "min_ping":
			servis, ping := getMinPing()
			response = "servis: " + servis + "; ping: " + ping.String()
		case "y":
			response = getStatistic()
		default:
			response = getPing(command)
		}

		if response != "Error! this servis not supporting" && command != "y"{
			commandStatistic[command] += 1
		}

		fmt.Println("Response: " + response)

		conn.Write([]byte(response))
	}
}

func getStatistic() (response string){
	response = "\n"
	for key, value := range commandStatistic{
		response += key+": " + fmt.Sprintf("%v",value) + "\n"
	}
	return response
}

func getPing(servisName string) string {
	for _, servis := range servicesInfo {
		if servis.name == servisName && servis.pingIsCheck {
			return servis.ping.String()
		}

		if servis.name == servisName && !servis.pingIsCheck {
			return "Error to connect servis: " + servis.name
		}
	}

	return "Error! this servis not supporting"
}

func getMaxPing() (string, time.Duration) {

	var maxPing time.Duration = 0 * time.Second
	var site string

	for _, service := range servicesInfo {
		if service.ping > maxPing && service.pingIsCheck {
			maxPing = service.ping
			site = service.name
		}
	}

	return site, maxPing
}

func getMinPing() (string, time.Duration) {

	var minPing time.Duration = 60 * time.Second
	var site string

	for _, servis := range servicesInfo {
		if servis.ping < minPing && servis.pingIsCheck {
			minPing = servis.ping
			site = servis.name
		}
	}
	fmt.Println(minPing.String())

	return site, minPing
}

func pingSitesThread() {
	for {
		for i := range servicesInfo {
			go checkUrl(&servicesInfo[i])
		}
		time.Sleep(5 * time.Minute)
	}
}

func setServisInfo() {
	file, err := os.Open("sites.txt")

	if err != nil {
		panic(err)
	}

	defer file.Close()

	reader := bufio.NewReader(file)

	for {
		line, err := reader.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				break
			} else {
				panic(err)
			}
		}
		servicesInfo = append(servicesInfo, 
			serviceInfo{name: strings.TrimSpace(line), ping: 0 * time.Second, pingIsCheck: false})
	}

	file.Close()
}

func checkUrl(service *serviceInfo) {
	start := time.Now()
	resp, err := http.Get("https://"+service.name)
	duration := time.Since(start)

	if err != nil {
		fmt.Println("Ошибка соединения.", err)
		service.pingIsCheck = false
		return
	}

	defer resp.Body.Close()

	if resp.StatusCode != 200 {

		fmt.Println("Ошибка. http-статус: ", resp.StatusCode)
		(*service).pingIsCheck = false
		return 
	}

	service.ping = duration
	service.pingIsCheck = true
}
