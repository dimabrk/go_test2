package main
import(
	"fmt"
	"net"
)

func main() {
	conn, err := net.Dial("tcp", "127.0.0.1:4545")

	if err != nil{
		panic("error connection")
	}

	defer conn.Close()
	for{
		var command string
		fmt.Print("Enter command (max_ping, min_ping) or servis name: ")
		_,err := fmt.Scanln(&command)

		if err != nil{
			fmt.Println("Bad command! Error", err)
			continue
		}

		if n, err := conn.Write([]byte(command)); n == 0 || err != nil{
			panic(err)
		}

		fmt.Print("Response: ")

		buff := make([]byte, 1024)
		n, err := conn.Read(buff)

		if err != nil{
			fmt.Print("Error")
			break
		}

		fmt.Print(string(buff[0:n]))

		fmt.Println()
	}
}